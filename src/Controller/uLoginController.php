<?php
/**
 * Created by PhpStorm.
 * User: felix
 * Date: 19.06.18
 * Time: 19:48
 */

namespace App\Controller;

use App\Entity\User;
use App\Model\Api\ApiContext;
use App\Model\Api\ApiException;
use App\Model\User\UserHandler;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class uLoginController extends Controller
{
    const ULOGIN_DATA = 'ulogin-data';

    /**
     * @Route("/register-case", name="app-register-case")
     */
    public function registerCaseAction()
    {
        $s = file_get_contents('http://ulogin.ru/token.php?token=' . $_POST['token'] . '&host=' . $_SERVER['HTTP_HOST']);
        $userData = json_decode($s, true);

        $user = new User();
        $user->setEmail($userData['email']);

        $form = $this->createForm(
            'App\Form\ULoginRegisterType', $user, [
                'action' => $this
                    ->get('router')
                    ->generate('app-register-case2')
            ]
        );
        //$user['network'] - соц. сеть, через которую авторизовался пользователь
        //$user['identity'] - уникальная строка определяющая конкретного пользователя соц. сети
        //$user['first_name'] - имя пользователя
        //$user['last_name'] - фамилия пользователя
        $this->get('session')->set(self::ULOGIN_DATA, $s);

        return $this->render('registration/ul_2_state.html.twig', [
            'form' => $form->createView(),
            'error' => null
        ]);
    }

    /**
     * @Route("/register-case2", name="app-register-case2")
     * @param ApiContext $apiContext
     * @param Request $request
     * @param ObjectManager $manager
     * @param UserHandler $userHandler
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function registerCase2Action(
        ApiContext $apiContext,
        Request $request,
        ObjectManager $manager,
        UserHandler $userHandler
    )
    {
        $userData = json_decode(
            $this
                ->get('session')
                ->get(self::ULOGIN_DATA),
            true
        );

        $user = new User();

        $form = $this->createForm(
            'App\Form\ULoginRegisterType',
            $user
        );

        $form->handleRequest($request);

        $error = null;
        if ($form->isSubmitted() && $form->isValid()) {
            try {
                if ($apiContext->clientExists($user->getPassport(), $user->getEmail())) {
                    $error = 'Error: Вы уже зарегистрированы. Вам нужно авторизоваться.';
                } else {
                    $user->setPassword("social------------------------".time());
                    $user->setSocialId($userData['network'], $userData['uid']);
                    $data = $user->__toArray();

                    $apiContext->createClient($data);

                    $user = $userHandler->createNewUser($data);

                    $manager->persist($user);
                    $manager->flush();

                    return $this->redirectToRoute("app-index");
                }
            } catch (ApiException $e) {
                $error = 'Error: ' . $e->getMessage() . '  |||  ' . var_export($e->getResponse(), 1);
            }
        }

        return $this->render('registration/ul_2_state.html.twig', [
            'form' => $form->createView(),
            'error' => $error
        ]);
    }

    /**
     * @Route("/add-soc-account", name="add-soc-account")
     * @param ObjectManager $manager
     * @param ApiContext $apiContext
     * @return Response
     * @throws ApiException
     *  @param Session $session
     */
    public function addSocialAccountAction(ObjectManager $manager, ApiContext $apiContext, Session $session){
        $s = file_get_contents('http://ulogin.ru/token.php?token=' . $_POST['token'] . '&host=' . $_SERVER['HTTP_HOST']);
        $user = json_decode($s, true);
        //$user['network'] - соц. сеть, через которую авторизовался пользователь
        //$user['identity'] - уникальная строка определяющая конкретного пользователя соц. сети
        //$user['first_name'] - имя пользователя
        //$user['last_name'] - фамилия пользователя

        /** @var User $currentUser */
        $currentUser = $this->getUser();

        $data = [
            'email' => $currentUser->getEmail(),
            'network' => $user['network'],
            'uid' => $user['uid']
        ];
        $currentUser->setSocialId($user['network'], $user['uid']);

        $error = null;

        try {
            if ($apiContext->addClientsSocialId($data)) {
                $currentUser = $this->getUser();
                $currentUser->setSocialId($user['network'], $user['uid']);
                $manager->persist($currentUser);
                $manager->flush();
                $session->getFlashBag()->set('message', "Аккаунт связан с соц.сетью {$user['network']}");
                return $this->redirectToRoute('app-my-page');

            } else {
                $error = 'Не найден';
            }
        } catch (ApiException $e) {
            if(isset($e->getResponse()['status']) && $e->getResponse()['status'] === 'error') {
                $userDataFromCentral = $apiContext->getClientByEmail($currentUser->getEmail());
                $currentUser->setFaceBookId($userDataFromCentral['faceBookId'])
                    ->setVkId($userDataFromCentral['vkId'])
                    ->setGoogleId($userDataFromCentral['googleId']);
                $manager->persist($currentUser);
                $manager->flush();
            }
            $error = $e->getResponse()['error'];
        }
        $session->getFlashBag()->set('error', $error);
        return $this->redirectToRoute('app-my-page');

    }





}